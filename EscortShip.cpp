#include "EscortShip.h"

namespace prog_4
{
	EscortShip::EscortShip(): Ship()
	{
		EscortShip::Ship::type = 2;
	}

	EscortShip::~EscortShip()
	{
		EscortShip::list_of_weapons.clear();
	}

	Weapon EscortShip::get_weapon(int index) const
	{
		if (index < 0 || index >= list_of_weapons.size())
			throw std::logic_error("EscortShip: invalid weapon index");
		return EscortShip::list_of_weapons[index];
	}

	void EscortShip::set_weapon(const Weapon & w, int index)
	{
		if (index <0 || index>EscortShip::list_of_weapons.size())
			throw std::logic_error("MilitaryShip: invalid weapon index");
		if (index != EscortShip::list_of_weapons.size())
			EscortShip::list_of_weapons[index] = w;
		else 
			EscortShip::list_of_weapons.push_back(w);
	}

	void EscortShip::delete_weapon(int index)
	{
		if (index < 0 || index >= EscortShip::list_of_weapons.size())
			throw std::logic_error("EscortShip:invalid weapon index");
		auto i = EscortShip::list_of_weapons.begin() + index;
		EscortShip::list_of_weapons.erase(i);
	}

	void EscortShip::shot(int index)
	{
		if (index < 0 || index >= EscortShip::list_of_weapons.size())
			throw std::logic_error("EscortShip:invalid weapon index");
		EscortShip::list_of_weapons[index].shot();
	}

	std::ostream & EscortShip::print(std::ostream & s) const
	{

		s << "EscortShip: " << std::endl;
		Ship::print(s);
		s << "Weapon: " << std::endl;
		for (int i = 0; i < EscortShip::list_of_weapons.size(); i++)
			s << EscortShip::list_of_weapons[i];
		s << std::endl;

		return s;
	}

	std::ofstream & EscortShip::fprint(std::ofstream & s) const
	{
		int len = list_of_weapons.size();
		Ship::fprint(s);
		s.write((char*)&len, 4);
		for (int i = 0; i < len; i++)
			s << EscortShip::list_of_weapons[i];
		return s;
	}

	std::ifstream & EscortShip::fread(std::ifstream & s)
	{
		int len;
		Ship::fread(s);
		s.read((char*)&len, 4);
		EscortShip::list_of_weapons.resize(len);
		for (int i = 0; i < len; i++)
			s >> EscortShip::list_of_weapons[i];
		return s;
	}

	std::ostream & operator<<(std::ostream & s, const EscortShip & a)
	{
		return a.print(s);
	}
	std::ofstream & operator<<(std::ofstream & s, const EscortShip & a)
	{
		return a.fprint(s);
	}
	std::ifstream & operator>>(std::ifstream & s, EscortShip & a)
	{
		return a.fread(s);
	}
}