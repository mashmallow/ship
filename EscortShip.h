#pragma once
#include "Ship.h"
#include "Weapon.h"

namespace prog_4 {
	class EscortShip : public Ship
	{
	private:
		vector<Weapon>list_of_weapons;
	public:
		EscortShip();

		Weapon get_weapon(int index) const;
		vector<Weapon> & get_weapon() {return EscortShip::list_of_weapons;}
		void set_weapon(const vector<Weapon> & list) { EscortShip::list_of_weapons = list;}
		void set_weapon(const Weapon & w, int index);
		void delete_weapon(int index);
		int get_quantity_of_weapon() const { return list_of_weapons.size(); }
		void shot(int index);

		friend class Table; // now class Table can access private members of a EscortShip

		std::ostream & print(std::ostream & s) const;
		virtual std::ofstream & fprint(std::ofstream & s) const;
		virtual std::ifstream & fread(std::ifstream & s);

		friend std::ofstream & operator<<(std::ofstream & s, const EscortShip & a);
		friend std::ifstream & operator>>(std::ifstream & s, EscortShip & a);
		friend std::ostream & operator << (std::ostream & s, const EscortShip & a);

		~EscortShip();
	};
}

