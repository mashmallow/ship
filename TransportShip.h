#pragma once
#include "Ship.h"
namespace prog_4 {
	class TransportShip:public Ship 
	{
	private:
		int cargo_weight;
		int addiction;  //the rule by how our velocity will change
	public:
		TransportShip();
		~TransportShip();

		void set_cargo_weight(int cargo_weight);
		void set_addiction(int addiction);

		int get_cargo_weight() const { return  TransportShip::cargo_weight; }
		int get_addiction() const;
		int get_current_speed() const;

		friend class Table; // now class Table can access private members of a TransportShip

		std::ostream & print(std::ostream & s) const;
		virtual std::ofstream & fprint(std::ofstream & s) const;
		virtual std::ifstream & fread(std::ifstream & s);

		friend std::ofstream & operator<<(std::ofstream & s, const TransportShip & a);
		friend std::ifstream & operator>>(std::ifstream & s, TransportShip & a);
		friend std::ostream & operator<<(std::ostream & s, const TransportShip & a);

	};
}

