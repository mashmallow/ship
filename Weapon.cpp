#include "Weapon.h"
namespace prog_4 {

	Weapon::Weapon()
	{
		Weapon::caliber = 1;
		Weapon::firing_range = 1;
		Weapon::Weapon_location = 1;
		Weapon::ammo = 0;
	}


	Weapon::~Weapon()
	{
	}

	std::string Weapon::name_of_weapon_location() const
	{
		std::string result;
		if (Weapon::Weapon_location == 1)
		{
			result = "nose"; // ���
		}
		else if (Weapon::Weapon_location == 2)
		{
			result = "left board"; // ����� ����
		}
		else if (Weapon::Weapon_location == 3)
		{
			result = "right board"; // ������ ����
		}
		else
		{
			result = "stern"; // �����
		}
		return result;
	}

	void Weapon::set_caliber(double caliber)
	{
		if (caliber <= 0)
		{
			throw std::logic_error("Weapon:invalid caliber");
		}
		Weapon::caliber = caliber;
	}

	void Weapon::set_firing_range(int firing_range)
	{
		if (firing_range <= 0)
		{
			throw std::logic_error("Weapon:invalid firing range");
		}
		Weapon::firing_range = firing_range;
	}

	void Weapon::set_weapon_location(int Weapon_location)
	{
		if (Weapon_location!=1 && Weapon_location != 2 && Weapon_location != 3 && Weapon_location != 4)
		{
			throw std::logic_error("Weapon:invalid Weapon location");
		}
		Weapon::Weapon_location = Weapon_location;
	}

	void Weapon::set_ammo(int ammo)
	{
		if (ammo < 0)
		{
			throw std::logic_error("Weapon:invalid ammo");
		}
		Weapon::ammo = ammo;
	}

	void Weapon::shot()
	{
		if (Weapon::ammo == 0)
		{
			throw std::logic_error("Weapon:you ammo is empty");
		}
		Weapon::ammo--;
	}

	std::ostream & operator<<(std::ostream & s, const Weapon & a)
	{
		s << "Weapon: ";
		s << "caliber: " << (int)a.get_caliber() << " mm ";
		s << "firing range: "<< a.get_firing_range() << " m ";
		s << "Weapon location: " << a.name_of_weapon_location() << " ";
		s << "ammo: " << a.get_ammo();
		s << std::endl;
		return s;
	}

	std::ifstream & operator>>(std::ifstream & s,Weapon & a)
	{
		s.read((char*)&(a.caliber), 4);
		s.read((char*)&(a.firing_range), 4);
		s.read((char*)&(a.Weapon_location), 4);
		s.read((char*)&(a.ammo), 4);
		return s;
	}

	std::ofstream & operator<<(std::ofstream & s,const Weapon & a)
	{
		s.write((char*)&(a.caliber), 4);
		s.write((char*)&(a.firing_range), 4);
		s.write((char*)&(a.Weapon_location), 4);
		s.write((char*)&(a.ammo), 4);
		return s;

	}

}