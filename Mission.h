#pragma once
#include "Fleet.h"
#include<iostream>
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::ios;

namespace prog_4 {
	class Mission
	{
	public:
		Mission();
		int main_dialog();
		~Mission();
	private:
		Fleet a;
		int choose_ship(Fleet & a);
		int get_number_of_ships_of_type(Fleet & a);
		int add_ship_to_Fleet(Fleet & a);
		int remove_ship_from_Fleet(Fleet & a);
		int upgrade_ship(Fleet & a);
		int load_cargo(Fleet & a);
		int carry_cargo_between_ships(Fleet & a);
		int vuvod_Fleet(Fleet & a);
		int save_Fleet(Fleet &a);
		int load_Fleet(Fleet &a);
		int set_point_of_departure(Fleet &a);
		int set_destination(Fleet &a);
		int set_distance(Fleet &a);

		int small_dialog(Fleet &a);

		int set_name(Fleet &a, int index);
		int set_captain(Fleet &a, int index);
		int set_displacement(Fleet &a, int index);
		int set_maximum_speed(Fleet &a, int index);
		int set_number_of_crew_members(Fleet &a, int index);
		int vuvod_ship(Fleet &a, int index);

		int set_rank(Fleet &a, int index);
		int set_first_name(Fleet &a, int index);
		int set_second_name(Fleet &a, int index);
		int set_third_name(Fleet &a, int index);
		int vuvod_captain(Fleet &a, int index);

		int actions_with_cargo(Fleet &a, int index);
		int set_cargo_weight(Fleet &a, int index);
		int set_addiction(Fleet &a, int index);

		int actions_with_weapon(Fleet &a, int index);
		int choose_weapon(Fleet &a, int index);
		int add_weapon(Fleet &a, int index);
		int delete_weapon(Fleet &a, int index);
		int get_quantity_of_weapon(Fleet &a, int index);
		int vuvod_weapon(Fleet &a, int index);
		int shot(Fleet &a, int index);

		int set_caliber(Fleet &a, int index, int index2);
		int set_firing_range(Fleet &a, int index, int index2);
		int set_weapon_location(Fleet &a, int index, int index2);
		int set_ammo(Fleet &a, int index, int index2);
		int vuvod_this_weapon(Fleet &a, int index, int index2);
	};
}

