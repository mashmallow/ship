#include <limits>
#include "Mission.h"
namespace prog_4 {

	Mission::Mission() = default;


	Mission::~Mission() = default;

	int Mission::main_dialog()
	{
			Fleet a;
			int control;
			int(Mission::*fptr[])(Fleet&) = { NULL, &Mission::choose_ship, &Mission::get_number_of_ships_of_type, &Mission::add_ship_to_Fleet, &Mission::remove_ship_from_Fleet, &Mission::upgrade_ship, &Mission::load_cargo, &Mission::carry_cargo_between_ships, &Mission::vuvod_Fleet, &Mission::save_Fleet, &Mission::load_Fleet, &Mission::set_point_of_departure, &Mission::set_destination, &Mission::set_distance };
			while (std::cout << "MENU!\n 1 - Choose a ship\n 2 - Get number of ships of special type\n 3 - Add ship to a fleet\n 4 - Remove ship from a fleet\n 5 - Upgrade a ship\n 6 - Load cargo\n 7 - Carry cargo between ships\n 8 - Output a fleet\n 9 - Save a fleet\n 10 - Load a fleet\n 11 - Set point of departure\n 12 - Set destination\n 13 - Set distance\n 14 - Exit\n" << std::endl)
			{
				std::cin >> control;
				if (!std::cin.good() || control < 1 || control>14) std::cout << "Error, repeat enter!" << std::endl; else
					if (control == 14) break; else (this->*fptr[control])(a);
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				std::cout << std::endl << std::endl;
			}
			std::cout << "End of work!" << std::endl;
			return 0;
	}
	int Mission::choose_ship(Fleet & a)
	{
		int index = small_dialog(a);
		if (index == -1) return 0;
		if (index >= a.get_table().get_length() || index < 0)
		{
			std::cout << "\tInvalid index";
			return 0;
		}
		//
		Ship * ptr = a.get_table().get_descriptor2(index);
		int type = ptr->get_type();
		//
		std::cout << *ptr << endl;
		int control;
		std::string choise[3];
		choise[0] = "Choose one option!\n1 - Set name\n2 - Set captain\n3 - Set Displacement\n4 - Set Max Speed\n5 - Output ship\n6 - Actions with cargo\n7 - exit";
		choise[1] = "Choose one option!\n1 - Set name\n2 - Set captain\n3 - Set Displacement\n4 - Set Max Speed\n5 - Output ship\n6 - Actions with weapon\n7 - exit";
		choise[2] = "Choose one option!\n1 - Set name\n2 - Set captain\n3 - Set Displacement\n4 - Set Max Speed\n5 - Output ship\n6 - Actions with cargo\n7 - actions with weapon\n8 - exit";
		int(Mission::*fptr11[])(Fleet&, int) = { NULL, &Mission::set_name, &Mission::set_captain, &Mission::set_displacement, &Mission::set_maximum_speed, &Mission::vuvod_ship,&Mission::actions_with_cargo };
		int(Mission::*fptr12[])(Fleet&, int) = { NULL, &Mission::set_name, &Mission::set_captain, &Mission::set_displacement, &Mission::set_maximum_speed, &Mission::vuvod_ship ,&Mission::actions_with_weapon };
		int(Mission::*fptr13[])(Fleet&, int) = { NULL, &Mission::set_name, &Mission::set_captain, &Mission::set_displacement, &Mission::set_maximum_speed, &Mission::vuvod_ship ,&Mission::actions_with_cargo,&Mission::actions_with_weapon };
		while (cout << choise[type - 1] << endl)
		{
			std::cin >> control;
			if (type == 1)
			{
				if (!std::cin.good() || control < 1 || control>8) std::cout << "Error, repeat enter!" << std::endl; else
					if (control == 8) break; else (this->*fptr11[control])(a, index);
			}
			else if (type == 2)
			{
				if (!std::cin.good() || control < 1 || control>8) std::cout << "Error, repeat enter!" << std::endl; else
					if (control == 8) break; else (this->*fptr12[control])(a, index);
			}
			else
			{
				if (!std::cin.good() || control < 1 || control>9) std::cout << "Error, repeat enter!" << std::endl; else
					if (control == 9) break; else (this->*fptr13[control])(a, index);
			}
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << std::endl << std::endl;
		}
		return 1;
	}
	int Mission::get_number_of_ships_of_type(Fleet & a)
	{
		int type;
		std::cout << "Enter type: 1 - TransportShip, 2 - EscortShip, 3 - MilitaryShip" << std::endl;
		std::cin >> type;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid type input";
			return 0;
		}
		try
		{
			std::cout << "Number of ships with this type: " << a.get_number_of(type);
		}
		catch (const std::exception&msg)
		{
			std::cout << msg.what() << std::endl;
		}
		return 1;
	}

	int Mission::add_ship_to_Fleet(Fleet & a)
	{
		int type, destination_distance;
		string call_sign;
		std::cout << "Enter type: 1 - TransportShip, 2 - EscortShip, 3 - MilitaryShip" << std::endl;
		std::cin >> type;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid type input";
			return 0;
		}
		if (type != 1 && type != 2 && type != 3)
		{
			std::cout << "\tInvalid type";
			return 0;
		}
		std::cout << "Enter call sign" << std::endl;
		std::cin >> call_sign;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid call sign input";
			return 0;
		}
		if (a.find(call_sign) != -1)
		{
			std::cout << "We have already got a ship with this name";
			return 0;
		}
		std::cout << "Enter destination_distance" << std::endl;
		std::cin >> destination_distance;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid destination_distance input";
			return 0;
		}
		Element el;
		el.call_sign = call_sign;
		el.destination_distance = destination_distance;
		switch (type)
		{
		case 1: {TransportShip * Ship1 = new TransportShip; el.descriptor = Ship1; break; }
		case 2: {EscortShip * Ship1 = new EscortShip; el.descriptor = Ship1; break; }
		case 3: {MilitaryShip * Ship1 = new MilitaryShip; el.descriptor = Ship1; break; }
		}
		a.set_element(el, a.get_table().get_length());
		return 1;
	}

	int Mission::remove_ship_from_Fleet(Fleet & a)
	{
		int index = small_dialog(a);
		if (index == -1) return 0;
		try
		{
			a.delete_element(index);
			cout << "A ship has been deleted" << endl;
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::upgrade_ship(Fleet & a)
	{

		std::cout << "Enter first index" << std::endl;
		int index1 = small_dialog(a);
		if (index1 == -1) return 0;
		std::cout << "Enter second index" << std::endl;
		int index2 = small_dialog(a);
		if (index2 == -1) return 0;
		try
		{
			a.upgrade(index1, index2);
			cout << "We  have upgraded this ship to MilitaryShip!" << endl;
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::load_cargo(Fleet & a)
	{
		int cargo;
		std::cout << "Enter cargo" << std::endl;
		std::cin >> cargo;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid cargo";
			return 0;
		}
		try
		{
			a.loading(cargo);
			cout << "We  have loaded cargo on our ships!" << endl;
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::carry_cargo_between_ships(Fleet & a)
	{
		int cargo;
		std::cout << "Enter cargo" << std::endl;
		std::cin >> cargo;
		if (!std::cin.good())
		{
			std::cout << "\tinvalid cargo";
			return 0;
		}
		std::cout << "Enter first index" << std::endl;
		int index1 = small_dialog(a);
		if (index1 == -1) return 0;
		std::cout << "Enter second index" << std::endl;
		int index2 = small_dialog(a);
		if (index2 == -1) return 0;
		try
		{
			a.carry(index1, index2, cargo);
			cout << "We have carried cargo between Ships!" << endl;
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::vuvod_Fleet(Fleet & a)
	{
		std::cout << "Output fleet" << std::endl;
		std::cout << a;
		return 1;
	}

	int Mission::small_dialog(Fleet & a)
	{
		int control;
		cout << "Push 1 to enter index, 2 to enter call sign" << endl;
		cin >> control;
		if (!std::cin.good() || (control != 1 && control != 2))
		{
			std::cout << "\tInvalid number";
			return -1;
		}
		if (control == 1)
		{
			int index;
			std::cout << "Enter index" << std::endl;
			std::cin >> index;
			if (!std::cin.good())
			{
				std::cout << "\tInvalid index";
				return -1;
			}
			return index;
		}
		else
		{
			std::string name;
			std::cout << "Enter name" << std::endl;
			std::cin >> name;
			if (!std::cin.good())
			{
				std::cout << "\tInvalid name" << endl;
				return -1;
			}
			int result = a.find(name);
			if (result == -1)
			{
				cout << "\tInvalid name" << endl;
				return -1;
			}
			else
				return result;
		}
	}
	int Mission::save_Fleet(Fleet & a)
	{
		std::cout << "Enter name of file" << endl;
		std::string name_of_file;
		std::cin >> name_of_file;
		if (!cin.good())
		{
			std::cout << "\tinvalid_vuvod" << std::endl;
			return 0;
		}
		std::ofstream s(name_of_file + ".bin", ios::binary);
		if (!s) std::cerr << "Cant open file \n";
		s << a;
		//cout << x;
		s.close();
		cout << "We wrote our fleet in file!" << endl;
		return 1;
	}

	int Mission::load_Fleet(Fleet & a)
	{
		//table c;
		std::cout << "Enter name of file" << endl;
		std::string name_of_file;
		std::cin >> name_of_file;
		if (!cin.good())
		{
			std::cout << "\tInvalid output" << std::endl;
			return 0;
		}
		std::ifstream s;
			s.open(name_of_file + ".bin", ios::binary);
		if (!s)
		{
			std::cerr << "Cant open file \n";
			return 0;
		}
		s >> a;
		cout << a;
		s.close();
		cout << "We got our fleet from file!";
		return 1;
	}
	int Mission::set_point_of_departure(Fleet & a)
	{
		string point_of_departure;
		cout << "Enter point_of_departure" << endl;
		cin >> point_of_departure;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid point_of_departure";
			return 0;
		}
		a.set_point_of_departure(point_of_departure);
		return 1;
	}
	int Mission::set_destination(Fleet & a)
	{
		string destination;
		cout << "Enter destination" << endl;
		cin >> destination;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid destination";
			return 0;
		}
		a.set_destination(destination);
		return 1;
	}
	int Mission::set_distance(Fleet & a)
	{
		int distance;
		cout << "Enter distance" << endl;
		cin >> distance;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid distance";
			return 0;
		}
		try
		{
			a.set_distance(distance);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
			return 0;
		}
		return 1;
	}
	int Mission::set_name(Fleet & a, int index)
	{
		std::string name;
		std::cout << "Enter name" << std::endl;
		std::cin >> name;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid name";
			return 0;
		}
		a.set_in_ship_str("Name", name, index);
		return 1;
	}

	int Mission::set_displacement(Fleet & a, int index)
	{
		int displacement;
		std::cout << "Enter displacement" << std::endl;
		std::cin >> displacement;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid displacement";
			return 0;
		}
		try
		{
			a.set_in_ship_int("Displacement ", displacement, index);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::set_captain(Fleet & a, int index)
	{
		std::cout << a.get_table().get_descriptor(index).get_captain() << endl;
		int control;
		int(Mission::*fptr2[])(Fleet&, int) = { NULL, &Mission::set_rank, &Mission::set_first_name, &Mission::set_second_name, &Mission::set_third_name, &Mission::vuvod_captain };
		while (std::cout << "Choose one option! 1 - Set rank\n 2 - Set first name\n 3 - Set second name,\n 4 - Set third name\n 5 - Output Captain\n 6 - Exit" << std::endl)
		{
			std::cin >> control;
			if (!std::cin.good() || control < 1 || control>7) std::cout << "Error, repeat enter!" << std::endl; else
				if (control == 7) break; else (this->*fptr2[control])(a, index);
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << std::endl << std::endl;
		}
		return 1;
	}

	int Mission::set_maximum_speed(Fleet & a, int index)
	{
		int maximum_speed;
		std::cout << "Enter MaxSpeed" << std::endl;
		std::cin >> maximum_speed;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid maximum_speed";
			return 0;
		}
		try
		{
			a.set_in_ship_int("MaxSpeed ", maximum_speed, index);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}


	int Mission::vuvod_ship(Fleet &a, int index)
	{
		cout << *a.get_table().get_descriptor2(index);
		return 1;
	}

	int Mission::set_rank(Fleet & a, int index)
	{
		std::string rank;
		std::cout << "Enter rank" << std::endl;
		std::cin >> rank;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid rank";
			return 0;
		}
		a.set_in_ship_captain_str("Rank", rank, index);
		return 1;
	}

	int Mission::set_first_name(Fleet & a, int index)
	{
		std::string first_name;
		std::cout << "Enter first_name" << std::endl;
		std::cin >> first_name;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid first_name";
			return 0;
		}
		a.set_in_ship_captain_str("First_name", first_name, index);
		return 1;
	}

	int Mission::set_second_name(Fleet & a, int index)
	{
		std::string second_name;
		std::cout << "Enter second_name" << std::endl;
		std::cin >> second_name;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid second_name";
			return 0;
		}
		a.set_in_ship_captain_str("Second_name", second_name, index);
		return 1;
	}

	int Mission::set_third_name(Fleet & a, int index)
	{
		std::string third_name;
		std::cout << "Enter third_name" << std::endl;
		std::cin >> third_name;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid third_name";
			return 0;
		}
		a.set_in_ship_captain_str("Third_name", third_name, index);
		return 1;
	}


	int Mission::vuvod_captain(Fleet & a, int index)
	{
		cout << a.get_table().get_descriptor(index).get_captain();
		return 1;
	}

	int Mission::actions_with_cargo(Fleet & a, int index)
	{
		Ship * ptr = a.get_table().get_descriptor2(index);
		int control;
		int(Mission::*fptr3[])(Fleet&, int) = { NULL, &Mission::set_cargo_weight, &Mission::set_addiction };
		while (std::cout << "Choose one option!\n1 - Set cargo weight\n2 - Set deceleraion rate\n3 - Exit\n" << std::endl)
		{
			std::cin >> control;
			if (!std::cin.good() || control < 1 || control>3) std::cout << "Error, repeat enter!" << std::endl; else
				if (control == 3) break; else (this->*fptr3[control])(a, index);
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << std::endl << std::endl;
		}
		return 1;
	}

	int Mission::set_cargo_weight(Fleet & a, int index)
	{
		int cargo_weight;
		std::cout << "Enter Cargo_weight" << std::endl;
		std::cin >> cargo_weight;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid cargo_weight";
			return 0;
		}
		try
		{
			a.set_in_transport_int("Cargo_weight", cargo_weight, index);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::set_addiction(Fleet & a, int index)
	{
		int addiction;
		std::cout << "Enter addiction" << std::endl;
		std::cin >> addiction;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid addiction";
			return 0;
		}
		try
		{
			a.set_in_transport_int("Addiction", addiction, index);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::actions_with_weapon(Fleet & a, int index)
	{
		Ship * ptr = a.get_table().get_descriptor2(index);
		int control;
		int(Mission::*fptr4[])(Fleet&, int) = { NULL, &Mission::choose_weapon, &Mission::add_weapon, &Mission::delete_weapon, &Mission::get_quantity_of_weapon,&Mission::vuvod_weapon, &Mission::shot };
		while (std::cout << "Choose one option!\n1 - Choose weapon\n2 - Add weapon\n3 - Delete weapon\n4 - Get quantity of weapon\n5 - Output weapon\n6 - Shot\n7 - Exit" << std::endl)
		{
			std::cin >> control;
			if (!std::cin.good() || control < 1 || control>7) std::cout << "Error, repeat enter!" << std::endl; else
				if (control == 7) break; else (this->*fptr4[control])(a, index);
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << std::endl << std::endl;
		}
		return 1;
	}

	int Mission::choose_weapon(Fleet & a, int index)
	{
		int index2;
		std::cout << "Enter index" << std::endl;
		std::cin >> index2;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid index";
			return 0;
		}
		Ship * ptr = a.get_table().get_descriptor2(index);
		if (ptr->get_type() == 2)
		{
			EscortShip * copy1 = dynamic_cast<EscortShip*>(ptr);
			if (index2 < 0 || index2 >= copy1->get_quantity_of_weapon())
			{
				cout << "\tInvalid index" << endl;
				return 0;
			}
			cout << copy1->get_weapon(index2) << endl;
		}
		else
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr);
			if (index2 < 0 || index2 >= copy1->get_quantity_of_weapon())
			{
				cout << "\tInvalid index" << endl;
				return 0;
			}
			cout << copy1->get_weapon(index2) << endl;
		}
		int control;
		int(Mission::*fptr5[])(Fleet&, int, int) = { NULL, &Mission::set_caliber, &Mission::set_firing_range, &Mission::set_weapon_location, &Mission::set_ammo, &Mission::vuvod_this_weapon };
		while (std::cout << "Choose one option!\n1 - Set caliber\n2 - Set firing range\n3 - Set weapon location\n4 - Set ammo\n5 -Output this weapon\t6 - Exit" << std::endl)
		{
			std::cin >> control;
			if (!std::cin.good() || control < 1 || control>6) std::cout << "Error, repeat enter!" << std::endl; else
				if (control == 6) break; else (this->*fptr5[control])(a, index, index2);
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << std::endl << std::endl;
		}
		return 1;
	}

	int Mission::add_weapon(Fleet & a, int index)
	{
		try
		{
			a.set_in_weapon("Add weapon", 0, index);
			cout << "Weapon have been added" << endl;
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
			return 0;
		}
		return 1;
	}

	int Mission::delete_weapon(Fleet & a, int index)
	{
		int index2;
		std::cout << "Enter index" << std::endl;
		std::cin >> index2;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid index";
			return 0;
		}
		try
		{
			a.set_in_weapon("Delete weapon", index2, index);
			cout << "Weapon have been deleted" << endl;
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
			return 0;

		}
		return 1;
	}

	int Mission::get_quantity_of_weapon(Fleet & a, int index)
	{
		cout << "Quantity of weapons: ";
		Ship * ptr = a.get_table().get_descriptor2(index);
		if (ptr->get_type() == 2)
		{
			EscortShip * copy1 = dynamic_cast<EscortShip*>(ptr);
			cout << copy1->get_quantity_of_weapon() << endl;
		}
		else
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr);
			cout << copy1->get_quantity_of_weapon() << endl;
		}
		return 1;
	}

	int Mission::vuvod_weapon(Fleet & a, int index)
	{
		cout << "weapon on this Ship: " << endl;
		Ship * ptr = a.get_table().get_descriptor2(index);
		if (ptr->get_type() == 2)
		{
			EscortShip * copy1 = dynamic_cast<EscortShip*>(ptr);
			cout << "quantity of weapon:  " << copy1->get_quantity_of_weapon() << endl;
			for (int i = 0; i < copy1->get_quantity_of_weapon(); i++)
				cout << copy1->get_weapon(i) << endl;
		}
		else
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr);
			cout << "quantity of weapon:  " << copy1->get_quantity_of_weapon() << endl;
			for (int i = 0; i < copy1->get_quantity_of_weapon(); i++)
				cout << copy1->get_weapon(i) << endl;
		}
		return 1;
	}

	int Mission::shot(Fleet & a, int index)
	{
		int index2;
		std::cout << "Enter index" << std::endl;
		std::cin >> index2;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid index";
			return 0;
		}
		try
		{
			a.set_in_weapon("shot", index2, index);
			cout << "PAW! PAW! PAW!" << endl;
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::set_caliber(Fleet & a, int index, int index2)
	{
		int caliber;
		std::cout << "Enter caliber" << std::endl;
		std::cin >> caliber;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid caliber";
			return 0;
		}
		try
		{
			a.set_in_weapon_double("Caliber", caliber, index, index2);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::set_firing_range(Fleet & a, int index, int index2)
	{
		int firing_range;
		std::cout << "Enter Firing range" << std::endl;
		std::cin >> firing_range;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid Firing_range";
			return 0;
		}
		try
		{
			a.set_in_weapon_int("Firing_range", firing_range, index, index2);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
		}
		return 1;
	}

	int Mission::set_weapon_location(Fleet & a, int index, int index2)
	{
		int weapon_location;
		std::cout << "Enter weapon_location" << std::endl;
		std::cin >> weapon_location;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid weapon_location";
			return 0;
		}
		try
		{
			a.set_in_weapon_int("Weapon_location", weapon_location, index, index2);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
			return 0;
		}
		return 1;
	}

	int Mission::set_ammo(Fleet & a, int index, int index2)
	{
		int ammo;
		std::cout << "Enter ammo" << std::endl;
		std::cin >> ammo;
		if (!std::cin.good())
		{
			std::cout << "\tInvalid ammo";
			return 0;
		}
		try
		{
			a.set_in_weapon_int("Ammo", ammo, index, index2);
		}
		catch (const std::exception&msg)
		{
			cout << msg.what() << endl;
			return 0;
		}
		return 1;
	}

	int Mission::vuvod_this_weapon(Fleet & a, int index, int index2)
	{
		Ship * ptr = a.get_table().get_descriptor2(index);
		if (ptr->get_type() == 2)
		{
			EscortShip * copy1 = dynamic_cast<EscortShip*>(ptr);
			cout << copy1->get_weapon(index2) << endl;
		}
		else
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr);
			cout << copy1->get_weapon(index2) << endl;
		}
		return 1;
	}


}
