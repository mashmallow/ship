#pragma once
#include <iostream>
#include <string>
#include <fstream>
namespace prog_4 {
class Weapon
{
private:
	double caliber;
	int firing_range;
	int Weapon_location;
	int ammo;
public:
	Weapon();
	std::string name_of_weapon_location() const;

	void set_caliber(double caliber);
	void set_firing_range(int firing_range);
	void set_weapon_location(int Weapon_location);
	void set_ammo(int ammo);

	double get_caliber() const { return Weapon::caliber; }
	int get_firing_range() const { return Weapon::firing_range; }
	int get_weapon_location() const { return Weapon::Weapon_location; }
	int get_ammo() const { return Weapon::ammo; }

	void shot();
	friend std::ostream & operator << (std::ostream & s, const Weapon & a);
	friend std::ifstream & operator >> (std::ifstream & s,  Weapon & a);
	friend std::ofstream & operator << (std::ofstream & s, const Weapon & a);

	friend class EscortShip; // now class EscortShip can access private members of a Captain
	friend class MilitaryShip; // now class MilitaryShip can access private members of a Captain

	~Weapon();

};

std::ostream & operator << (std::ostream & s, const Weapon & a);
}


// define могут если что служить причиной ошибок
// для перенсимости мб нужно будет заменить pragma на ifndef/define
// мб потом в сеттерах поставить чтобы они что то возвращали
// рабобраться с капитаном конвоя
// во 2 гет декскрипторе мб надо поставить const ы еще где то
// сделать clone для вставки в это
// тейбл сет элемент, если там где этот деструктор для этой хуйни, может вылетать
// сделать итератор и конст итератор для вектора шаблона

// тесты test1 для тестирования загрузки груза
// test2 показывает что все поля работают


//

