#include "Ship.h"

namespace prog_4 {

	Ship::Ship()
	{
		Captain a;
		Ship::name = "No name";
		Ship::Ship_captain = a;
		Ship::displacement = 1;
		Ship::maximum_speed = 1;
		Ship::type = 0;
	}

	void Ship::set_name(std::string name)
	{
		Ship::name = name;
	}

	void Ship::set_displacement(int displacement)
	{
		if (displacement <= 0)
		{
			throw std::logic_error("Ship:invalid displacement");
		}
		Ship::displacement = displacement;
	}

	void Ship::set_maximum_speed(int maximum_speed)
	{
		if (maximum_speed <= 0)
		{
			throw std::logic_error("Ship:invalid maximum speed");
		}
		Ship::maximum_speed = maximum_speed;
	}


	std::ostream & Ship::print(std::ostream & s) const
	{
		s << "Ship name: " << Ship::name << std::endl;
		s << Ship_captain;
		s << "Displacement: " << Ship::displacement << " tn ";
		s << "MaxSpeed: " << Ship::maximum_speed << " m/s ";
		s << std::endl;

		return s;
	}
	std::ofstream & Ship::fprint(std::ofstream & s) const
	{
		s << Ship::name.length() << Ship::name ;
		s << Ship::Ship_captain;
		s.write((char*)&(Ship::displacement), 4);
		s.write((char*)&(Ship::maximum_speed), 4);
		s.write((char*)&(Ship::type), 4);

		return s;
	}
	std::ifstream & Ship::fread(std::ifstream & s) 
	{
		char buf1[80];
		int len1;
		s >> len1;
		s.read(buf1, len1);
		Ship::name = buf1;
		Ship::name.resize(len1);
		s >> Ship::Ship_captain;
		s.read((char*)&(Ship::displacement), 4);
		s.read((char*)&(Ship::maximum_speed), 4);
		s.read((char*)&(Ship::type), 4);
		return s;
	}
	std::ostream & operator<<(std::ostream & s, const Ship & a)
	{
		return a.print(s);
	}
	std::ofstream & operator<<(std::ofstream & s, const Ship & a)
	{
		return a.fprint(s);
	}
	std::ifstream & operator>>(std::ifstream & s, Ship & a)
	{
		return a.fread(s);
	}
}
