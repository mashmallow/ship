#include "TransportShip.h"

namespace prog_4 {

	TransportShip::TransportShip():Ship()
	{
		TransportShip::Ship::type = 1;
		TransportShip::cargo_weight = 0;
		TransportShip::addiction = 1;
	}

	TransportShip::~TransportShip() { }

	void TransportShip::set_cargo_weight(int cargo_weight)
	{
		if (cargo_weight < 0)
		{
			throw std::logic_error("TransportShip:invalid cargo_weight");
		}
		TransportShip::cargo_weight = cargo_weight;
	}

	void TransportShip::set_addiction(int addiction)
	{
		TransportShip::addiction = addiction;
	}

	int TransportShip::get_addiction() const
	{
		return TransportShip::addiction; }

	int TransportShip::get_current_speed() const
	{
		if (TransportShip::maximum_speed - TransportShip::addiction*TransportShip::cargo_weight>=0)
		 return TransportShip::maximum_speed - TransportShip::addiction*TransportShip::cargo_weight;
		else return 0; }

	std::ostream & TransportShip::print(std::ostream & s) const
	{
		s << "TransportShip: " << std::endl;
		Ship::print(s);
		s << "Cargo_weight: " << TransportShip::cargo_weight << " kg ";
		s << "Addiction: " << TransportShip::addiction << " ";
		s << "Current speed: " << TransportShip::get_current_speed() << " ";
		s << std::endl;
		return s;
	}

	std::ofstream & TransportShip::fprint(std::ofstream & s) const
	{
		Ship::fprint(s);
		s.write((char*)&(TransportShip::cargo_weight), 4);
		s.write((char*)&(TransportShip::addiction), 4);
		return s;
	}

	std::ifstream & TransportShip::fread(std::ifstream & s)
	{
		Ship::fread(s);
		s.read((char*)&(TransportShip::cargo_weight), 4);
		s.read((char*)&(TransportShip::addiction), 4);
		return s;
	}

	std::ostream & operator<<(std::ostream & s, const TransportShip & a)
	{
		return a.print(s);
	}

	std::ofstream & operator<<(std::ofstream & s, const TransportShip & a)
	{
		return a.fprint(s);
	}

	std::ifstream & operator>>(std::ifstream & s, TransportShip & a)
	{
		return a.fread(s);
	}
}