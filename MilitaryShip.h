#pragma once
#include "Ship.h"
#include "Weapon.h"

namespace prog_4 {
	class MilitaryShip :public Ship
	{
    private:
        int cargo_weight;
        int addiction;
        vector<Weapon>list_of_weapons;
	public:
		MilitaryShip();
		~MilitaryShip();

		void set_cargo_weight(int cargo_weight);
		void set_addiction(int addiction);

		int get_cargo_weight() const { return  MilitaryShip::cargo_weight; }
		int get_addiction() const;

		friend class Table;// now class Table can access private members of a MilitaryShip

		int get_current_speed() const;

		Weapon get_weapon(int index) const;
		void set_weapon(const Weapon & w, int index);
		vector<Weapon> get_weapon() { return MilitaryShip::list_of_weapons; } const
		void set_weapon(const vector<Weapon> & list) { MilitaryShip::list_of_weapons = list; }
		void delete_weapon(int index);
		int get_quantity_of_weapon() const { return MilitaryShip::list_of_weapons.size(); }
		void shot(int index);

		std::ostream & print(std::ostream & s) const;
		virtual std::ofstream & fprint(std::ofstream & s) const;
		virtual std::ifstream & fread(std::ifstream & s);

		friend std::ofstream & operator<<(std::ofstream & s, const MilitaryShip & a);
		friend std::ifstream & operator>>(std::ifstream & s, MilitaryShip & a);
		friend std::ostream & operator << (std::ostream & s, const MilitaryShip & a);

	};
}

