#pragma once
#include<iostream>
namespace prog_4
{
	template <class T>
	class vector_it
	{
	private:
		T *cur;
	public:
		vector_it();
		vector_it(T * it) : cur(it) {}
		~vector_it();
		int operator ==(const vector_it<T> & it) const;
		int operator !=(const vector_it<T> & it) const;
		vector_it <T> operator = (const vector_it<T> & it);
		const T & operator *();
		vector_it<T> & operator ++();
		vector_it<T> operator ++(int);
		vector_it<T> operator + (int chislo) const;
		vector_it<T> operator - (int chislo) const;
		int operator - (const vector_it<T> & it2) const;

	};

	template <class T>
	vector_it<T>::vector_it()
	{
		vector_it::cur = nullptr;
	}

	template <class T>
	vector_it<T>::~vector_it()
	{ }

	template <class T>
	int vector_it<T>::operator==(const vector_it<T> & it) const
	{
		return vector_it::cur == it.cur;
	}

	template <class T>
	int vector_it<T>::operator!=(const vector_it<T> & it) const
	{
		return vector_it::cur != it.cur;
	}

	template<class T>
	inline vector_it<T> vector_it<T>::operator=(const vector_it<T>& it)
	{
		vector_it::cur = it;
		return *this;
	}

	template <class T>
	const T & vector_it<T>::operator*()
	{
		if (vector_it::cur)
			return *cur;
		throw std::logic_error("invalid value for vector iterator");
	}

	template <class T>
	vector_it<T> & vector_it<T>::operator++()
	{
		++vector_it::cur;
		return *this;
	}

	template <class T>
	vector_it<T> vector_it<T>::operator++(int)
	{
		vector_it res;
		res.cur = vector_it::cur;
		++cur;
		return res;
	}

	template<class T>
	vector_it<T> vector_it<T>::operator+(int chislo) const
	{
		vector_it<T> result(vector_it::cur + chislo);
		return result;
	}

	template<class T>
	vector_it<T> vector_it<T>::operator-(int chislo) const
	{
		vector_it<T> result(vector_it::cur + chislo);
		return result;
	}

	template<class T>
	int vector_it<T>::operator-(const vector_it<T> & it2) const
	{
		return vector_it::cur - it2.cur;
	}

	template <class T>
	class vector_const_it
	{
	public:
		vector_const_it();
		vector_const_it(T * it) : cur(it) {}
		~vector_const_it();
		int operator ==(const vector_const_it<T> & it) const;
		int operator !=(const vector_const_it<T> & it) const;
		vector_const_it<T> operator = (const vector_const_it<T> & it);
		vector_const_it<T> & operator ++();
		vector_const_it<T> operator ++(int);
		const T & operator *();
		vector_const_it<T> operator + (int chislo) const;
		vector_const_it<T> operator - (int chislo) const;
		int operator - (const vector_const_it<T> & it2) const;
	private:
		const T *cur;
	};

	template <class T>
	vector_const_it<T>::vector_const_it()
	{
		vector_const_it::cur = nullptr;
	}

	template <class T>
	vector_const_it<T>::~vector_const_it()
	{ }

	template <class T>
	int vector_const_it<T>::operator==(const vector_const_it<T> & it) const
	{
		return vector_const_it::cur == it.cur;
	}

	template <class T>
	int vector_const_it<T>::operator!=(const vector_const_it<T> & it) const
	{
		return vector_const_it::cur != it.cur;
	}

	template<class T>
	inline vector_const_it<T> vector_const_it<T>::operator=(const vector_const_it<T>& it)
	{
		vector_const_it::cur = it;
		return *this;
	}

	template <class T>
	vector_const_it<T> & vector_const_it<T>::operator++()
	{
		++vector_const_it::cur;
		return *this;
	}

	template <class T>
	vector_const_it<T> vector_const_it<T>::operator++(int)
	{
		vector_const_it res;
		res.cur = vector_const_it::cur;
		++cur;
		return res;
	}

	template <class T>
	const T & vector_const_it<T>::operator*()
	{
		if (vector_const_it::cur)
			return *cur;
		throw std::logic_error("invalid value for vector iterator");
	}


	template<class T>
	vector_const_it<T> vector_const_it<T>::operator+(int chislo) const
	{
		vector_const_it<T> result(vector_const_it::cur + chislo);
		return result;
	}

	template<class T>
	vector_const_it<T> vector_const_it<T>::operator-(int chislo) const
	{
		vector_const_it<T> result(vector_const_it::cur + chislo);
		return result;
	}

	template<class T>
	int vector_const_it<T>::operator-(const vector_const_it<T> & it2) const
	{
		return vector_const_it::cur - it2.cur;
	}




	template <class T> class vector
	{
	private:
		int length;
		T *massiv;
	public:
		friend class vector_it<T>;
		typedef vector_it<T> iterator;
		friend class vector_const_it<T>;
		typedef vector_const_it<T> const_iterator;
		vector();
		~vector();
		vector(const int & size);
		vector(const int * massiv, int size);
		vector(const vector<T> & vector); 
		vector(vector<T> && vector);
		vector<T> operator =(vector<T> vector2);
		int size() const;
		void resize(int len);
		void push_back(const T & element);
		void clear();
		T & operator[](const int & index);
		T operator[](const int & index) const;
		void erase(const iterator & it);
		iterator begin() {return iterator(vector::massiv); }
		iterator end() {return iterator(vector::massiv+vector::length); }
		const_iterator begin() const { return const_iterator(vector::massiv); }
		const_iterator end() const { return const_iterator(vector::massiv + vector::length); }

	};
	template<class T>
	vector<T>::vector()
	{
		vector::massiv = nullptr;
		vector::length = 0;
	}
	template<class T>
	vector<T>::~vector()
	{
		delete[] vector::massiv;
		vector::length = 0;
		vector::massiv = nullptr;
	}
	
	template<class T>
	vector<T>::vector(const int & size)
	{
			vector::length = size;
			vector::massiv = new T[vector::length];
	}
	
	template<class T>
	vector<T>::vector(const int * massiv, int size)
	{
		vector::length = size;
		vector::massiv = new T[vector::length];
			for (int i = 0; i < vector::length; i++)
			{
				vector::massiv[i] = massiv[i];
			}
	}
	
	template<class T>
	vector<T>::vector(const vector<T>& vector)
	{
		vector::clear();
		vector::length = vector.length;
		vector::massiv = new T[vector::length];
		for (int i = 0; i < vector::length; i++)
			vector::massiv[i] = vector.massiv[i];
	}
	
	template<class T>
	vector<T>::vector(vector<T>&& vector)
	{
		vector::length = vector.length;
		vector::massiv = vector.massiv;
		vector.massiv = nullptr;
	}

	template<class T>
	inline vector<T> prog_4::vector<T>::operator=(vector<T> vector2)
	{
		vector::clear();
		vector::length = vector2.length;
		vector::massiv = new T[vector::length];
		for (int i = 0; i < vector::length; i++)
			vector::massiv[i] = vector2.massiv[i];
		return *this;
	}
	
	template<class T>
	int vector<T>::size() const
	{
		return vector::length;
	}
	template<class T>
	inline void vector<T>::resize(int len)
	{
		int minlen;
		T * ptr = new T[len];
		if (len < vector::length) minlen = len; else minlen = vector::length;
		for (int i = 0; i < minlen; i++)
			ptr[i] = vector::massiv[i];
		for (int i = minlen; i < len; i++)
			ptr[i] = T();
		if (vector::massiv != nullptr && vector::length != 0)
			delete[]vector::massiv;
		vector::massiv = ptr;
		vector::length = len;
	}
	template<class T>
	void vector<T>::push_back(const T & element)
	{
		T * ptr;
		ptr = new T[vector::length + 1];
		for (int i = 0; i < vector::length; i++)
		{
			ptr[i] = vector::massiv[i];
		}
		ptr[vector::length] = element;
		vector::length++;
		delete[] vector::massiv;
		vector::massiv = ptr;
	}
	template<class T>
	void vector<T>::clear()
	{
		if (vector::massiv != nullptr && vector::length != 0)
				delete[] vector::massiv;
		vector::length = 0;
		vector::massiv = nullptr;
	}
	template<class T>
	T & vector<T>::operator[](const int & index)
	{
		return vector::massiv[index];
	}
	template<class T>
	T  vector<T>::operator[](const int & index) const
	{
		return vector::massiv[index];
	}

	template<class T>
	void vector<T>::erase(const iterator & it)
	{
		int index = it - vector::begin();
		for (int i = index; i < vector::length - 1; i++)
			vector::massiv[i] = vector::massiv[i + 1];
		T * ptr = new T[vector::length - 1];
		for (int i = 0; i < vector::length - 1; i++)
		{
			ptr[i] = vector::massiv[i];
		}
		vector::length--;
		delete[] vector::massiv;
		vector::massiv = ptr;
	}

}
