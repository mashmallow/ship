#include "Fleet.h"
namespace prog_4 {

	Fleet::Fleet()
	{
		Captain a;
		Fleet::Fleet_captain = a;
		Fleet::point_of_departure = "None";
		Fleet::destination = "None";
		Fleet::distance = 0;
	}


	Fleet::~Fleet()
	{
		Fleet::Table1.~Table();
	}

	void Fleet::set_in_ship_int(const std::string & s, const int & info, const int & index)
	{
		Ship * ptr = Fleet::Table1.mas[index].descriptor;
		if (s == "Displacement")
			ptr->set_displacement(info);
		else if (s == "Maximum_speed")
			ptr->set_maximum_speed(info);
		else
		throw std::logic_error("invalid name of what to change in Ship");
	}

	void Fleet::set_in_ship_str(const std::string & s, const std::string & info, const int & index)
	{
		if (s == "name")
			Fleet::Table1.mas[index].descriptor->set_name(info);
		else
			throw std::logic_error("invalid name of what to change in Ship");
	}

	void Fleet::set_in_ship_captain_str(const std::string & s, const std::string & info, const int & index)
	{
		Ship * ptr = Fleet::Table1.mas[index].descriptor;
		Captain copy = ptr->get_captain();
		if (s == "rank")
			copy.set_rank(info);
		else if (s == "first_name")
			copy.set_first_name(info);
		else if (s == "second_name")
			copy.set_second_name(info);
		else if (s == "third_name")
			copy.set_third_name(info);
		else throw std::logic_error("invalid name of what to change in captain");
		ptr->set_captain(copy);
	}
	void Fleet::set_in_transport_int(const std::string & s, const int & info, const int & index)
	{
		Ship * ptr = Fleet::Table1.mas[index].descriptor;
		if (ptr->get_type() == 1)
		{
			TransportShip * copy1 = dynamic_cast<TransportShip*>(ptr);
			if (s == "cargo_weight")
				copy1->set_cargo_weight(info);
			else if (s == "addiction")
				copy1->set_addiction(info);
			else throw std::logic_error("invalid name of what to change in Ship with cargo");
		}
		else if (ptr->get_type() == 3)
		{
			MilitaryShip * copy2 = dynamic_cast<MilitaryShip*>(ptr);
			if (s == "cargo_weight")
				copy2->set_cargo_weight(info);
			else if (s == "addiction")
				copy2->set_addiction(info);
			else throw std::logic_error("invalid name of what to change in Ship with cargo");
		}
		else throw std::logic_error("this Ship is not cargo");
	}
	void Fleet::set_in_weapon(const std::string & s, const int & info, const int & index)
	{
		Ship * ptr = Fleet::Table1.mas[index].descriptor;
		if (ptr->get_type() == 2)
		{
			EscortShip * copy1 = dynamic_cast<EscortShip*>(ptr);
			if (s == "add_weapon")
			{
				Weapon w;
				copy1->set_weapon(w, copy1->get_quantity_of_weapon());
			}
			else if (s == "delete_weapon")
				copy1->delete_weapon(info);
			else if (s == "shot")
				copy1->shot(info);
			else throw std::logic_error("invalid name of what to do in Ship with weapon");
		}
		else if (ptr->get_type() == 3)
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr);
			if (s == "add_weapon")
			{
				Weapon w;
				copy1->set_weapon(w, copy1->get_quantity_of_weapon());
			}
			else if (s == "delete_weapon")
				copy1->delete_weapon(info);
			else if (s == "shot")
				copy1->shot(info);
			else throw std::logic_error("invalid name of what to do in Ship with weapon");
		}
		else throw std::logic_error("this Ship is not have weapon");
	}
	void Fleet::set_in_weapon_int(const std::string & s, const int & info, const int & index, const int & index2)
	{
		Weapon w;
		Ship * ptr = Fleet::Table1.mas[index].descriptor;
		if (ptr->get_type() == 2)
		{
			EscortShip * copy1 = dynamic_cast<EscortShip*>(ptr);
			w = copy1->get_weapon(index2);
			if (s == "firing_range")
			{
				w.set_firing_range(info);
				copy1->set_weapon(w, index2);
			}
			else if (s == "weapon_location")
			{
				w.set_weapon_location(info);
				copy1->set_weapon(w, index2);
			}
			else if (s == "ammo")
			{
				w.set_ammo(info);
				copy1->set_weapon(w, index2);
			}
			else throw std::logic_error("invalid name of what to do in this weapon");
		}
		else if (ptr->get_type() == 3)
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr);
			w = copy1->get_weapon(index2);
			if (s == "firing_range")
			{
				w.set_firing_range(info);
				copy1->set_weapon(w, index2);
			}
			else if (s == "weapon_location")
			{
				w.set_weapon_location(info);
				copy1->set_weapon(w, index2);
			}
			else if (s == "ammo")
			{
				w.set_ammo(info);
				copy1->set_weapon(w, index2);
			}
			else throw std::logic_error("invalid name of what to do in this weapon");
		}
		else throw std::logic_error("this Ship is not have weapon");
	}
	void Fleet::set_in_weapon_double(const std::string & s, const double & info, const int & index, const int & index2)
	{
		Weapon w;
		Ship * ptr = Fleet::Table1.mas[index].descriptor;
		if (ptr->get_type() == 2)
		{
			EscortShip * copy1 = dynamic_cast<EscortShip*>(ptr);
			w = copy1->get_weapon(index2);
			if (s == "caliber")
			{
				w.set_caliber(info);
				copy1->set_weapon(w, index2);
			}
			else throw std::logic_error("invalid name of what to do in this weapon");
		}
		else if (ptr->get_type() == 3)
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr);
			w = copy1->get_weapon(index2);
			if (s == "caliber")
			{
				w.set_caliber(info);
				copy1->set_weapon(w, index2);
			}
			else throw std::logic_error("invalid name of what to do in this weapon");
		}
		else throw std::logic_error("this Ship is not have weapon");
	}
	void Fleet::set_element(const Element & el, int index)
	{
		Fleet::Table1.set_Element(el, index);
	}
	void Fleet::delete_element(int index)
	{
		Fleet::Table1.delete_Element(index);
	}
	void Fleet::upgrade(int index1, int index2)
	{
		if (index1 < 0 || index2 < 0 || index1 >= Fleet::Table1.get_length() || index2 >= Fleet::Table1.get_length() || index1 == index2)
			throw std::logic_error("invalid index");
		Ship *p1 = Fleet::Table1.mas[index1].descriptor;
		Ship *p2 = Fleet::Table1.mas[index2].descriptor;
		if (p1->get_type() != 1)
			throw std::logic_error("first Ship is not transport Ship");
		if (p2->get_type() != 2 && p2->get_type() != 3)
			throw std::logic_error("second Ship do not have weapon");
		MilitaryShip * ptr = new MilitaryShip;
		TransportShip * copy1 = dynamic_cast<TransportShip*>(p1);
		if (p2->get_type() == 2)
		{
			EscortShip * copy2 = dynamic_cast<EscortShip*>(p2);
			if (!copy1 || !copy2)
				throw std::logic_error("Fleet:illegal conversation");
			ptr->set_captain(p1->get_captain());
			ptr->set_displacement(p1->get_displacement());
			ptr->set_maximum_speed(p1->get_maximum_speed());
			ptr->set_name(p1->get_name());
			ptr->set_cargo_weight(copy1->get_cargo_weight());
			ptr->set_addiction(copy1->get_addiction());
			for (int i = 0;i< copy2->get_quantity_of_weapon();i++)
				ptr->set_weapon(copy2->get_weapon(i), i);
			for (; 0 < copy2->get_quantity_of_weapon(); )
				copy2->delete_weapon(0);
			Fleet::Table1.mas[index1].descriptor = ptr;
		}
		else if (p2->get_type() == 3)
		{
			MilitaryShip * copy2 = dynamic_cast<MilitaryShip*>(p2);
			if (!copy1 || !copy2)
				throw std::logic_error("Fleet:illegal conversation");
			ptr->set_captain(p1->get_captain());
			ptr->set_displacement(p1->get_displacement());
			ptr->set_maximum_speed(p1->get_maximum_speed());
			ptr->set_name(p1->get_name());
			ptr->set_cargo_weight(copy1->get_cargo_weight());
			ptr->set_addiction(copy1->get_addiction());
			for (int i = 0; i < copy2->get_quantity_of_weapon(); i++)
				ptr->set_weapon(copy2->get_weapon(i), i);
			for (; 0 < copy2->get_quantity_of_weapon(); )
				copy2->delete_weapon(0);
			Fleet::Table1.mas[index1].descriptor = ptr;
		}
	}
	int Fleet::Fleet_speed()
	{
		int v = 0, sv = 0;
		Ship * s;
		int minv = 2147483647;
		if (Fleet::Table1.get_length() == 0) return 0;
		for (int i = 0; i < Fleet::Table1.get_length(); i++)
		{
			s = Fleet::Table1.mas[i].descriptor;
			if (s->get_type() == 1)
			{
				TransportShip * s2 = dynamic_cast<TransportShip*>(s);
				sv = s2->get_current_speed();
			}
			else if (s->get_type() == 2)
			{
				EscortShip * s2 = dynamic_cast<EscortShip*>(s);
				sv = s2->get_maximum_speed();
			}
			else if (s->get_type() == 3)
			{
				MilitaryShip * s2 = dynamic_cast<MilitaryShip*>(s);
				sv = s2->get_current_speed();
			}
			if (sv < minv)
				minv = sv;
		}
		return minv;
	}
	int Fleet::find(std::string name)
	{
		for (int i = 0; i < Fleet::Table1.get_length(); i++)
			if (Fleet::Table1.mas[i].call_sign == name)
				return i;
		return -1;
	}
	int Fleet::get_number_of(int type) const
	{
		if (type != 1 && type != 2 && type != 3)
			throw std::logic_error("invalid type");
		int result = 0;
		for (int i = 0; i < Fleet::Table1.get_length(); i++)
		{
			if (Fleet::Table1.mas[i].descriptor->get_type() == type)
				result++;
		}
		return result;
	}
	void Fleet::carry(int index1, int index2, int weight)
	{
		if (index1 < 0 || index1 >= Fleet::Table1.get_length())
			throw std::logic_error("Fleet: bad 1 index");
		if (index2 < 0 || index2 >= Fleet::Table1.get_length())
			throw std::logic_error("Fleet: bad 2 index");
		Ship * ptr1 = Fleet::Table1.mas[index1].descriptor;
		Ship * ptr2 = Fleet::Table1.mas[index2].descriptor;
		if (ptr1->get_type() == 2)
			throw std::logic_error("Fleet: 1 is not a transport");
		if (ptr2->get_type() == 2)
			throw std::logic_error("Fleet: 2 is not a transport");
		if (ptr1->get_type() == 1 && ptr2->get_type() == 1)
		{
			TransportShip * copy1 = dynamic_cast<TransportShip*>(ptr1);
			TransportShip * copy2 = dynamic_cast<TransportShip*>(ptr2);
			if (copy1->get_cargo_weight() < weight)
				throw std::logic_error("there is not so much cargo weight on 1 Ship");
			copy1->set_cargo_weight(copy1->get_cargo_weight() - weight);
			copy2->set_cargo_weight(copy2->get_cargo_weight() + weight);
		}
		else if (ptr1->get_type() == 1 && ptr2->get_type() == 3)
		{
			TransportShip * copy1 = dynamic_cast<TransportShip*>(ptr1);
			MilitaryShip * copy2 = dynamic_cast<MilitaryShip*>(ptr2);
			if (copy1->get_cargo_weight() < weight)
				throw std::logic_error("there is not so much cargo weight on 1 Ship");
			copy1->set_cargo_weight(copy1->get_cargo_weight() - weight);
			copy2->set_cargo_weight(copy2->get_cargo_weight() + weight);
		}
		else if (ptr1->get_type() == 3 && ptr2->get_type() == 1)
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr1);
			TransportShip * copy2 = dynamic_cast<TransportShip*>(ptr2);
			if (copy1->get_cargo_weight() < weight)
				throw std::logic_error("there is not so much cargo weight on 1 Ship");
			copy1->set_cargo_weight(copy1->get_cargo_weight() - weight);
			copy2->set_cargo_weight(copy2->get_cargo_weight() + weight);
		}
		else if (ptr1->get_type() == 3 && ptr2->get_type() == 3)
		{
			MilitaryShip * copy1 = dynamic_cast<MilitaryShip*>(ptr1);
			MilitaryShip * copy2 = dynamic_cast<MilitaryShip*>(ptr2);
			if (copy1->get_cargo_weight() < weight)
				throw std::logic_error("there is not so much cargo weight on 1 Ship");
			copy1->set_cargo_weight(copy1->get_cargo_weight() - weight);
			copy2->set_cargo_weight(copy2->get_cargo_weight() + weight);
		}
	}
	int Fleet::loading(int weight)
	{
		if (Fleet::Table1.get_length() == 0)
			throw std::logic_error("our Fleet is empty");
		int maxv = 0;
		int index = 0;
		int cv;
		Ship * s = nullptr;
		for (int i = 1; i <= weight; i++)
		{
			maxv = 0;
			for (int j = 0; j < Fleet::Table1.get_length(); j++)
			{
				cv = 0;
				s = Fleet::Table1.mas[j].descriptor;
				if (s->get_type() == 1 || s->get_type() == 3)
				{
					if (s->get_type() == 1)
					{
						TransportShip * s2 = dynamic_cast<TransportShip*>(s);
						s2->set_cargo_weight(s2->get_cargo_weight() + 1);
						cv = Fleet::Fleet_speed();
						s2->set_cargo_weight(s2->get_cargo_weight() - 1);
					}
					else if (s->get_type() == 3)
					{
						MilitaryShip * s2 = dynamic_cast<MilitaryShip*>(s);
						s2->set_cargo_weight(s2->get_cargo_weight() + 1);
						cv = Fleet::Fleet_speed();
						s2->set_cargo_weight(s2->get_cargo_weight() - 1);
					}
					if (cv > maxv)
					{
						maxv = cv;
						index = j;
					}
				}
			}
			s = Fleet::Table1.get_descriptor2(index);
			if (s->get_type() == 1)
			{
				TransportShip * s2 = dynamic_cast<TransportShip*>(Fleet::Table1.get_descriptor2(index));
				if (s2 == nullptr) 
				{
					std::cout << "nullptr error! index = " << index << std::endl;
					std::cout << *(Fleet::Table1.get_descriptor2(index));
					return 0;
				}
				else
				s2->set_cargo_weight(s2->get_cargo_weight() + 1);
			}
			else if (s->get_type() == 3)
			{
				MilitaryShip * s2 = dynamic_cast<MilitaryShip*>(Fleet::Table1.get_descriptor2(index));
				s2->set_cargo_weight(s2->get_cargo_weight() + 1);
			}
		}
	}
	std::ostream & operator<<(std::ostream & s, const Fleet & a)
	{
		s << "Fleet" << std::endl;
		s << "captain of Fleet: " << a.Fleet_captain << " point_of_departure: " << a.point_of_departure << " destination: " << a.destination << " distance: " << a.distance << std::endl;
		s << a.Table1;
		return s;
	}
	std::ifstream & operator>>(std::ifstream & s, Fleet & a)
	{
		s >> a.Fleet_captain;
		char buf1[80];
		int len1;
		s.read((char*)&len1, 4);
		s.read(buf1, len1);
		a.point_of_departure = buf1;
		a.point_of_departure.resize(len1);

		char buf2[80];
		int len2;
		s.read((char*)&len2, 4);
		s.read(buf2, len2);
		a.destination = buf2;
		a.destination.resize(len2);

		s.read((char*)&a.distance, 4);
		s >> a.Table1;
		return s;
	}
	std::ofstream & operator<<(std::ofstream & s, const Fleet & a)
	{
		int pd = a.point_of_departure.length(), de = a.destination.length(), di = a.distance;
		s << a.Fleet_captain; 
		s.write((char*)&(pd), 4);
		s << a.point_of_departure;
		s.write((char*)&(de), 4);
		s << a.destination;
		s.write((char*)&(di), 4);
		s << a.Table1;
		return s;
	}
}
