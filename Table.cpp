#include "Table.h"

namespace prog_4 {
	Table::Table()
	{}


	Table::~Table()
	{
		for (int i = 0; i < Table::mas.size();i++)
			delete Table::mas[i].descriptor;
		Table::mas.clear();
	}
	void Table::set_Element(const Element & el, int index)
	{
		if (index <0 || index>Table::mas.size())
			throw std::logic_error("Table:invalid Element index");
		if (index != Table::mas.size())
		{
			delete Table::mas[index].descriptor;
			Table::mas[index] = el;
		}
		else
		{
			Table::mas.push_back(el);
		}
	}
	Ship Table::get_descriptor(const std::string & call_sign) const
	{
		for (int i = 0; i < Table::mas.size(); i++)
		{
			if (call_sign == Table::mas[i].call_sign)
				return *Table::mas[i].descriptor;
		}

		throw std::logic_error("Table:invalid call_sign");
	}
	Ship Table::get_descriptor(int index) const
	{
		if (index >= 0 && index < Table::mas.size())
			return *Table::mas[index].descriptor;;
		throw std::logic_error("Table:invalid index");
	}
	Ship * Table::get_descriptor2(int index) const
	{
		if (index >= 0 && index < Table::mas.size())
			return Table::mas[index].descriptor;;
		throw std::logic_error("Table:invalid index");
	}
	void Table::delete_Element(int index)
	{
		if (index < 0 || index >= Table::mas.size())
			throw std::logic_error("Table:invalid Element index");
		vector<Element>::iterator i = Table::mas.begin()+index;
		Table::mas.erase(i);
	}

/*
	Table::it Table::begin() const
	{
		return Table::it(Table::mas);
	}
	Table::it Table::end() const
	{
		return it(Table::mas + Table::mas.size());
	}
*/

	std::ostream & operator<<(std::ostream & s, const Table & a)
	{
		s << "Number of ships: " << a.get_length() << std::endl;
		for (vector<Element>::const_iterator it = a.mas.begin(); it != a.mas.end(); ++it)
		{
			s << "Ship: " << (*it).call_sign << " " << "Destination_distance: " << (*it).destination_distance << std::endl;
			(*it).descriptor->print(s);
		}
		return s;
	}

	std::ifstream & operator>>(std::ifstream & s, Table & a)
	{
		//
		int len;
		s.read((char*)&len, 4);
		a.mas.clear();
		a.mas.resize(len);
		for (int i = 0; i < len; i++)
		{
			int type;
			//
			char buf1[80];
			int len1;
			s.read((char*)&(len1), 4);
			s.read(buf1, len1);
			a.mas[i].call_sign = buf1;
			a.mas[i].call_sign.resize(len1);
			//
			s.read((char*)&(a.mas[i].destination_distance), 4);
			s.read((char*)&(type), 4);
			if (type == 1) a.mas[i].descriptor = new TransportShip;
			else if (type == 2) a.mas[i].descriptor = new EscortShip;
			else if (type == 3) a.mas[i].descriptor = new MilitaryShip;
			else throw std::logic_error("invalid type");
				a.mas[i].descriptor->fread(s);
		}
		return s;
	}

	std::ofstream & operator<<(std::ofstream & s, const Table & a)
	{
		int len = a.mas.size();
		s.write((char*)&len, 4);
		for (vector<Element>::const_iterator it = a.mas.begin(); it != a.mas.end(); ++it)
		{
			int cs = (*it).call_sign.length(), dd = (*it).destination_distance, t = (*it).descriptor->get_type();
			s.write((char*)&cs, 4);
			s << (*it).call_sign;
			s.write((char*)&dd, 4);
			s.write((char*)&t, 4);
			(*it).descriptor->fprint(s);
		}
		return s;
	}

	Table_it::Table_it()
	{
		Table_it::cur = nullptr;
	}


	Table_it::~Table_it()
	{
	}
	int Table_it::operator==(const Table_it & it) const
	{
		return Table_it::cur == it.cur;
	}
	int Table_it::operator!=(const Table_it & it) const
	{
		return Table_it::cur != it.cur;
	}
	const Element & Table_it::operator*()
	{
		if (Table_it::cur)
			return *cur;
		throw std::logic_error("invalid value for Table iterator");
	}
	Table_it & Table_it::operator++()
	{
		++Table_it::cur;
		return *this;
	}
	Table_it Table_it::operator++(int)
	{
		Table_it res;
		res.cur = Table_it::cur;
		++cur;
		return res;
	}
}
