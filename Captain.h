#pragma once
#include <iostream>
#include <string>
#include <fstream>

namespace prog_4 {

	class Captain
	{
	private:
		std::string rank;
		std::string first_name;
		std::string second_name;
		std::string third_name;
	public:

		Captain();
		Captain (std::string A, std::string B, std::string C, std::string D);
		
		void set_rank(const std::string & rank) { Captain::rank = rank; }
		void set_first_name(const std::string & first_name) { Captain::first_name = first_name; }
		void set_second_name(const std::string & second_name) { Captain::second_name = second_name; }
		void set_third_name(const std::string & third_name) { Captain::third_name = third_name; }

		std::string get_rank() const { return Captain::rank; }
		std::string get_first_name() const { return Captain::first_name; }
		std::string get_second_name() const { return Captain::second_name; }
		std::string get_third_name() const { return Captain::third_name; }

		// the non-member function operator<< will have access to Captain's private members
		friend std::ostream & operator << (std::ostream & s, const Captain & a);
		friend std::ofstream & operator << (std::ofstream & s, const Captain & a);
		friend std::ifstream & operator >> (std::ifstream & s, Captain & a);


		friend class Ship;  // now class Ship can access private members of a Captain
		friend class Fleet;  //now class Fleet can access private members os a Captain

		~Captain();
	};

}


