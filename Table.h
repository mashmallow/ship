#pragma once
#include "TransportShip.h"
#include "EscortShip.h"
#include "MilitaryShip.h"
#include "vector.h"

namespace prog_4 {

	struct Element
	{
		Ship * descriptor;
		std::string call_sign; 
		int destination_distance; 
	};


	class Table
	{
	private:
		vector<Element> mas;
	public:
		Table();
		~Table();
		
		void set_Element(const Element &el, int index);
	 
		int get_length() const { return Table::mas.size(); }
		Ship get_descriptor(const std::string & call_sign) const;
		Ship get_descriptor(int index) const;
		Ship * get_descriptor2(int index) const;
		
		void delete_Element(int index);
		
		friend class Fleet; // now class Fleet can access private members of a Table

		friend std::ostream & operator<<(std::ostream & s, const Table & a);
		friend std::ifstream & operator >> (std::ifstream & s, Table & a);
		friend std::ofstream & operator << (std::ofstream & s, const Table & a);

		friend class Table_it;
		//typedef Table_it it;
		//it begin() const;
		//it end() const;

	};

//итератор  //в итоге итератор работает у нас также как и стандартные библиотеки за счёт  методов(перегрузок операций)
	class Table_it
	{
	private:
		const Element *cur;
	public:
		Table_it();
		Table_it(const Element * it) : cur(it) {}  // инициализируем куррент итератором, который туда положили
		~Table_it();
		int operator ==(const Table_it & it) const;
		int operator !=(const Table_it & it) const;
		const Element & operator *();
		Table_it & operator ++();
		Table_it operator ++(int);
	};
}

