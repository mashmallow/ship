#pragma once
#include "Table.h"
namespace prog_4 {
	class Fleet
	{
	private:
		Table Table1;
		Captain Fleet_captain;
		std::string point_of_departure;
		std::string destination;
		int distance;
		int Fleet_speed();

	public:
		Fleet();

		void set_captain(const Captain & captain) { Fleet::Fleet_captain = captain; }
		void set_point_of_departure(const std::string & point_of_departure) { Fleet::point_of_departure = point_of_departure; }
		void set_destination(const std::string & destination) { Fleet::destination = destination; }
		void set_distance(const int & distance) { if (distance >= 0) Fleet::distance = distance; else throw std::logic_error("invalid distance"); } // >0
		void set_table(const Table & Table1) { Fleet::Table1 = Table1; }

		void set_in_ship_int(const std::string & s, const int & info, const int & index);
		void set_in_ship_str(const std::string & s, const std::string & info, const int & index);
		void set_in_ship_captain_str(const std::string & s, const std::string & info, const int & index);
		void set_in_transport_int(const std::string & s, const int & info, const int & index);
		void set_in_weapon(const std::string & s, const int & info, const int & index);
		void set_in_weapon_int(const std::string & s, const int & info, const int & index, const int & index2);
		void set_in_weapon_double(const std::string & s, const double & info, const int & index, const int & index2);

		void set_element(const Element &el, int index);
		void delete_element(int index);

		Captain get_captain() const { return Fleet::Fleet_captain; }
		std::string get_point_of_departure() const { return Fleet::point_of_departure; }
		std::string get_destination() const { return Fleet::destination; }
		int get_distance() const { return Fleet::distance; }
		const Table & get_table() const { return Fleet::Table1; }

		int find(std::string name);

		int get_number_of(int type) const;
		void carry(int index1, int index2, int weight);
		void upgrade(int index1, int index2);
		int loading(int weight);

		friend std::ostream & operator<<(std::ostream & s, const Fleet & a);
		friend std::ifstream & operator >> (std::ifstream & s, Fleet & a);
		friend std::ofstream & operator << (std::ofstream & s, const Fleet & a);

		~Fleet();

	};
}

