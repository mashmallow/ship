#include "Captain.h"

namespace prog_4
{
	Captain::Captain()
	{
		Captain::rank = "--";
		Captain::first_name = "--";
		Captain::second_name = "--";
		Captain::third_name = "--";
	}

	Captain::Captain(std::string A, std::string B, std::string C, std::string D) {
		Captain::rank = std::move(A);
		Captain::first_name = std::move(B);
		Captain::second_name = std::move(C);
		Captain::third_name = std::move(D);
	}

	Captain::~Captain() = default;

    std::ostream & operator << (std::ostream & s, const Captain & a)
	{
		s << "Captain: ";
		s << a.first_name << " " << a.second_name << " " << a.third_name << " ";
		s << "Rank: " << a.rank << " ";
		s << std::endl;
		return s;
	}
	std::ofstream & operator<<(std::ofstream & s, const Captain & a)
	{
		int s1 = a.first_name.length(), s2 = a.second_name.length(), s3 = a.third_name.length(), s4 = a.rank.length();
		s.write((char*)&s1, 4); // write to outfile
		s << a.first_name;
		s.write((char*)&s2, 4);
		s  << a.second_name;
		s.write((char*)&s3, 4);
		s  << a.third_name;
		s.write((char*)&s4, 4);
		s  << a.rank ;
		return s;
	}

	std::ifstream & operator>>(std::ifstream & s, Captain & a)
	{
        //allocate memory for file content
		char buf1[80], buf2[80], buf3[80], buf4[80];
		int len1, len2, len3, len4;
		s.read((char*)&len1, 4); //read content of infile
		s.read(buf1, len1);
		a.first_name = buf1;
		a.first_name.resize(len1);
		s.read((char*)&len2, 4);
		s.read(buf2, len2);
		a.second_name = buf2;
		a.second_name.resize(len2);
		s.read((char*)&len3, 4);
		s.read(buf3, len3);
		a.third_name = buf3;
		a.third_name.resize(len3);
		s.read((char*)&len4, 4);
		s.read(buf4, len4);
		a.rank = buf4;
		a.rank.resize(len4);
		return s;
	}


}