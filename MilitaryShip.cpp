#include "MilitaryShip.h"
namespace prog_4 {
	MilitaryShip::MilitaryShip()
	{
		MilitaryShip::Ship();
		MilitaryShip::Ship::type = 3;
		MilitaryShip::cargo_weight = 0;
		MilitaryShip::addiction = 1;
	}


	MilitaryShip::~MilitaryShip()
	{
		MilitaryShip::list_of_weapons.clear();
	}

	void MilitaryShip::set_cargo_weight(int cargo_weight)
	{
		if (cargo_weight < 0)
		{
			throw std::logic_error("MilitaryShip:invalid cargo_weight");
		}
		MilitaryShip::cargo_weight = cargo_weight;
	}

	void MilitaryShip::set_addiction(int addiction)
	{
		MilitaryShip::addiction = addiction;
	}

	int MilitaryShip::get_addiction() const
	{
		return MilitaryShip::addiction;
	}

	int MilitaryShip::get_current_speed() const
	{
		if (MilitaryShip::maximum_speed - MilitaryShip::addiction*MilitaryShip::cargo_weight >= 0)
			return MilitaryShip::maximum_speed - MilitaryShip::addiction*MilitaryShip::cargo_weight;
		else return 0;
	}

	Weapon MilitaryShip::get_weapon(int index) const
	{
		if (index < 0 || index >= MilitaryShip::list_of_weapons.size())
			throw std::logic_error("MilitaryShip:invalid weapon index");
		return MilitaryShip::list_of_weapons[index];
	}

	void MilitaryShip::set_weapon(const Weapon & w, int index)
	{
		if (index <0 || index>MilitaryShip::list_of_weapons.size())
			throw std::logic_error("MilitaryShip:invalid weapon index");
		if (index != MilitaryShip::list_of_weapons.size())
			MilitaryShip::list_of_weapons[index] = w;
		else
			MilitaryShip::list_of_weapons.push_back(w);
	}

	void MilitaryShip::delete_weapon(int index)
	{
		if (index < 0 || index >= MilitaryShip::list_of_weapons.size())
			throw std::logic_error("escort_Ship:invalid weapon index");
		auto i = MilitaryShip::list_of_weapons.begin() + index;
		MilitaryShip::list_of_weapons.erase(i);
	}

	void MilitaryShip::shot(int index)
	{
		if (index < 0 || index >= MilitaryShip::list_of_weapons.size())
			throw std::logic_error("MilitaryShip:invalid weapon index");
		MilitaryShip::list_of_weapons[index].shot();
	}

	std::ostream & MilitaryShip::print(std::ostream & s) const
	{
		s << "MilitaryShip: " << std::endl;

		Ship::print(s);

		s << "Cargo_weight: " << MilitaryShip::cargo_weight << " kg ";
		s << "Addiction: " << MilitaryShip::addiction << " ";
		s << "Current speed: " << MilitaryShip::get_current_speed() << " ";
		s << "Weapon: " << std::endl;
		for (int i = 0; i < MilitaryShip::list_of_weapons.size(); i++)
			s << MilitaryShip::list_of_weapons[i];
		s << std::endl;
		return s;
	}
	std::ostream & operator<<(std::ostream & s, const MilitaryShip & a)
	{
		return a.print(s);
	}

	std::ofstream & MilitaryShip::fprint(std::ofstream & s) const
	{
		int len = list_of_weapons.size();
		Ship::fprint(s);
		s.write((char*)&(MilitaryShip::cargo_weight), 4);
		s.write((char*)&(MilitaryShip::addiction), 4);
		s.write((char*)&len, 4);
		for (int i = 0; i < len; i++)
			s << MilitaryShip::list_of_weapons[i];
		return s;
	}
	std::ifstream & MilitaryShip::fread(std::ifstream & s)
	{
		int len;
		Ship::fread(s);
		s.read((char*)&(MilitaryShip::cargo_weight), 4);
		s.read((char*)&(MilitaryShip::addiction), 4);
		s.read((char*)&len, 4);
		list_of_weapons.resize(len);
		for (int i = 0; i < len; i++)
			s >> MilitaryShip::list_of_weapons[i];
		return s;
	}
	std::ofstream & operator<<(std::ofstream & s, const MilitaryShip & a)
	{
		return a.fprint(s);
	}
	std::ifstream & operator>>(std::ifstream & s, MilitaryShip & a)
	{
		return a.fread(s);
	}
}

