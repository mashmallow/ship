#pragma once
#include"Captain.h"
#include<vector>
using std::vector;
//#include"vector.h"

namespace prog_4 {
	class Ship
	{
	protected:
		std::string name;
		Captain Ship_captain;
		int displacement;
		int maximum_speed;
		int type;

	public:
		Ship();
		virtual ~Ship() {};

		void set_name(std::string name);
		void set_displacement(int displacement);
		void set_captain(const Captain & captain)
		{
			Ship::Ship_captain = captain;
		}
		void set_maximum_speed(int maximum_speed);

		std::string get_name()const { return Ship::name; }
		Captain get_captain() const { return Ship::Ship_captain; }
		int get_displacement() const { return Ship::displacement; }
		int get_maximum_speed() const { return  Ship::maximum_speed; }
		int get_type() const { return Ship::type; }

		
		friend class Table; // now class Table can access private members of a Ship
		virtual std::ostream & print(std::ostream & s) const;
		virtual std::ofstream & fprint(std::ofstream & s) const;
		virtual std::ifstream & fread(std::ifstream & s) ;

		friend std::ostream & operator<<(std::ostream & s, const Ship & a);
		friend std::ofstream & operator<<(std::ofstream & s, const Ship & a);
		friend std::ifstream & operator>>(std::ifstream & s,  Ship & a);


	};
}

